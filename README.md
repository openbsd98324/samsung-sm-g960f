 

 
Möglichkeit 1: Power- und Lautstärke-Taste für Galaxy S9 Screenshot verwenden

Für einen Screenshot mit dem Galaxy S9 kannst Du zum Beispiel gleichzeitig die Power- und die Leiser-Taste betätigen. Halte den Druck solange aufrecht, bis Du den Auslöseton der Kamera hörst oder das Display kurz aufleuchtet. Der Screenshot wird somit angefertigt und in Deiner Galerie abgespeichert. Anschließend kannst Du ihn bearbeiten und zum Beispiel in den sozialen Netzwerken teilen.


